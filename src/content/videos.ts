export const VIDEO_ID = '-0Hn51crcwU'

export const videos = [
  {
    id: 'wzypjHjOmEI',
    title: 'OLDSCHOOL RESURGE',
    shortDesc: 'A nossa viagem ao Algarve para visitar velhos amigos.'
  },
  {
    id: '1rx5n2ZTyU0',
    title: 'PARKOUR ESCAPE',
    shortDesc: 'Vídeo de parkour em formato POV com história e acção.'
  },
  {
    id: 'dQwYbP27BaQ',
    title: 'TRACE JAM 2022',
    shortDesc: 'Vídeo com os highlights da Trace Jam 2022 em Santo André'
  }
]
