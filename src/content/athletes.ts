export type Athlete = {
  avatar: string
  name: string
  handle: string
}

export const athletes: Athlete[] = [
  {
    avatar: 'cris.jpg',
    name: 'Cristóvão Morais',
    handle: 'cristovao.morais'
  },
  {
    avatar: 'arraia.jpg',
    name: 'Daniel Arraia',
    handle: 'thearraialife'
  },
  {
    avatar: 'lod.jpg',
    name: 'Rafael "Lod" Santos',
    handle: 'rafaelcanical'
  },
  {
    avatar: 'vitalii.jpg',
    name: 'Vitalii Vitruk',
    handle: 'vitruk.life'
  }
]
