import React from 'react'
import { VIDEO_ID } from '../content/videos'

const LastVideo = () => {
  return (
    <div className="last-video">
      {/* DESCRIPTION */}
      <div className="desc">
        <h3 className="strong-titles">Bem-vindos</h3>
        <h1 className="main-title">Nós somos os Trace Project uma equipa de parkour focada no movimento!</h1>
        <a href="/contacts" className="btn" style={{ marginTop: '40px' }}>
          Contacte-nos
        </a>
      </div>
      {/* THUMBNAIL */}
      <div className="video-thumbnail">
        <div className="responsive">
          <img src={`https://img.youtube.com/vi/${VIDEO_ID}/maxresdefault.jpg`} />
        </div>
        <a href={`https://youtu.be/${VIDEO_ID}`} target="_blank">
          Watch
        </a>
      </div>
    </div>
  )
}

export default LastVideo
