import React from 'react'
import { athletes } from '../content/athletes'

export default () => {
  return (
    <div className="the-team">
      <h2 className="strong-titles">A equipa</h2>
      <div className="athletes">
        {athletes.map((item) => (
          <div className="athlete" key={item.handle}>
            <img src={`/${item.avatar}`} alt={item.name} />
            <h3>{item.name}</h3>
            <a href={`https://instagram.com/${item.handle}`} target="_blank">
              @{item.handle}
            </a>
          </div>
        ))}
      </div>
    </div>
  )
}
