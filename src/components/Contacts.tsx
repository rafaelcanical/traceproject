import React, { useState } from 'react'

export default () => {
  const test = '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'

  // Local state
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [subject, setSubject] = useState('')
  const [message, setMessage] = useState('')
  const [nameInv, setNameInv] = useState(false)
  const [emailInv, setEmailInv] = useState(false)
  const [subjectInv, setSubjectInv] = useState(false)
  const [messageInv, setMessageInv] = useState(false)
  const [isSuccess, setSuccess] = useState(false)
  const [isError, setError] = useState(false)

  /**
   * Send an email to ourselves
   */
  const sendEmail = (e) => {
    e.preventDefault()

    setNameInv(false)
    setEmailInv(false)
    setSubjectInv(false)
    setMessageInv(false)
    setSuccess(false)
    setError(false)

    if (!name.length || !email.length || !subject.length || !message.length) {
      setError(true)

      if (!name.length) {
        setNameInv(true)
      }
      if (!email.length) {
        setEmailInv(true)
      }
      if (!subject.length) {
        setSubjectInv(true)
      }
      if (!message.length) {
        setMessageInv(true)
      }
    } else {
      const url = 'https://api.sendinblue.com/v3/smtp/email'

      // Post body data
      const body = {
        sender: {
          name,
          email
        },
        to: [
          {
            email: 'info.traceproject@gmail.com',
            name: 'Trace Project'
          }
        ],
        subject,
        htmlContent: `<html>
            <head></head>
            <body>
              <p>Enviado pelo site:</p>
              <p><strong>Mensagem:</strong> ${message}</p>
            </body>
          </html>`
      }

      // Request options
      const options = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'api-key': test
        }
      }

      // Send POST request
      fetch(url, options)
        .then((res) => res.json())
        .then((res) => {
          setSuccess(true)
          setName('')
          setEmail('')
          setSubject('')
          setMessage('')
        })
    }
  }

  return (
    <div className="contacts">
      <h1 className="big-titles">Contactos</h1>
      <p className="sentence1">
        Estamos sempre disponiveís no{' '}
        <a href="https://instagram.com/traceproject.pt" target="_blank">
          Instagram
        </a>
        .
      </p>
      <p className="sentence2">
        Envie-nos um email para
        <br /> <a href="mailto:info.traceproject@gmail.com">info.traceproject@gmail.com</a>
      </p>
      <form onSubmit={sendEmail} className="form">
        {isError ? <div className="error">Preencha todos os campos</div> : null}
        {isSuccess ? (
          <div className="success">Obrigado pelo seu contacto. Prometemos ser breves a responder.</div>
        ) : null}
        <div>
          <input
            value={name}
            className={nameInv ? 'invalid' : ''}
            type="text"
            placeholder="Nome"
            onChange={(e) => setName(e.target.value)}
          />
          <input
            value={email}
            className={emailInv ? 'invalid' : ''}
            type="email"
            required
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <input
          value={subject}
          className={subjectInv ? 'invalid' : ''}
          type="text"
          placeholder="Assunto"
          onChange={(e) => setSubject(e.target.value)}
        />
        <textarea
          value={message}
          className={messageInv ? 'invalid' : ''}
          name=""
          id=""
          cols="30"
          rows="10"
          placeholder="Mensagem"
          onChange={(e) => setMessage(e.target.value)}
        ></textarea>
        <button className="btn" type="submit">
          Send email
        </button>
      </form>
    </div>
  )
}
