import React from 'react'

export default () => {
  return (
    <div className="about-us">
      {/* DESCRIPTION */}
      <div className="desc">
        <h1 className="big-titles">Sobre nós</h1>
        <p>
          O projecto nasceu a 11 de Agosto de 2011. Este projecto tinha como principal objectivo criar um Futuro para o
          Parkour a nível Nacional. Isto é, pretendiamos desenvolver a mentalidade das pessoas de forma a aceitarem o
          Parkour como desporto, e tornar uma modalidade com visibilidade assim como acontece pelo resto do Mundo.
        </p>
        <p>
          Começamos por criar um evento que aconteceria todos os anos, era então chamado de Move West porque era sempre
          localizado na região Oeste de Portugal. Todos os anos mais e mais atletas se juntavam a este grande encontro.
        </p>
        <p>
          Após alguns anos, o grupo tornou-se mais curto apenas com o Cristóvão e o Lod. Depois disso deixamos de
          organizar o Move West. Um par de anos mais tarde, juntou-se a nós o Arraia para nos ajudar com a nossa visão e
          começamos novamente um grande evento anual. Desta vez decidimos chamar-lhe Trace Jam, já que o grupo
          simplificou de nome de Tracing Project para Trace Project.
        </p>
        <p>
          Precisavamos de um par de mãos extra desde que o Lod se mudou para a Noruega, e não era capaz de ajudar tanto
          à distância no sentido físico. Foi então que convidamos o Vitalii para nos ajudar a crescer e reiniciar a
          Trace Jam já que saltamos alguns anos durante da pandemia.
        </p>
      </div>
      <div className="images">
        <div>
          <img src="/image1.jpg" className="image1" />
          <img src="/image3.jpg" className="image3" />
        </div>
        <div>
          <img src="/image2.jpg" className="image2" />
          <img src="/image4.jpg" className="image4" />
        </div>
      </div>
    </div>
  )
}
